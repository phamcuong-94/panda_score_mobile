final String serverAddress = "http://pc-pay.qlkh-pru.com/";


/* -------------------------- API Nhân Viên ------------------------------- */
final String urlListNhanVien = "user";
final String urlUpdateNhanVien = "user/update";
final String urlDeleteNhanVien = "user/delete";
final String urlLogin = "login/login";
final String urlFrefreshToken = "login/refresh";
final String urlThongTinCaNhanNhanVien = "thong-tin-nhan-vien";
final String urlListHoaDon = "hoa-don";
final String urlChiTietHoaDon = "hoa-don/chi-tiet-hoa-don?id=";
final String urlHuyHoaDon = "hoa-don/huy-hoa-don?id=";
final String urlThemHoaDon = "hoa-don/update";
final String urlListHoaDonCustomer = "hoa-don/hoa-don-customer";
final String urlThanhToanHoaDon = "hoa-don/thanh-toan";
final String urlUserChangePassword = "user/change-password";
final String urlThongKeThang = "thong-ke/doanh-thu-thang";
/* -------------------------- API Nhân Viên ------------------------------- */
final String urlListSanPham = "san-pham";
final String urlListTheNap = "the-nap";
final String urlUpdateTheNap = "the-nap/update";
final String urlNapTheHo = "the-nap/nap-the-ho";
/* -------------------------- API Khách Hàng ------------------------------- */
final String urlKhachHangLogin = "login-khach-hang/login";
final String urlListKhachHang = "khach-hang";
final String urlUpdateKhachHang = "khach-hang/update";
final String urlThongTinCaNhanKhachHang = "thong-tin-khach-hang";
final String urlUpdateThongTinKhachHang = "thong-tin-khach-hang/update";
final String urlSaveTokenDevice = "user/save-token-device";
final String urlHoaDonByKhachHang = "hoa-don/hoa-don-by-khach-hang";
final String urlTheNapByKhachHang = "the-nap/the-nap-by-khach-hang";
final String urlNapThe = "the-nap/nap-the";
/* -------------------------- API Khách Hàng ------------------------------- */