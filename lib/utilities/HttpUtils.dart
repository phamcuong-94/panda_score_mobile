import 'dart:convert';
import 'package:http/http.dart' as http;

class HttpUtils {

  static login({String link, Map<String, dynamic> formData}) async {
    final http.Response response = await http.post(link,
        body: json.encode(formData),
        headers: {'Content-Type': 'application/json'});
    final Map<String, dynamic> mapResponse = await json.decode(response.body);
    return mapResponse;
  }

  static methodGET({String link, String token}) async {
    try {
      final http.Response response = await http.get(link,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
          });
      final Map<String, dynamic> mapResponse = await json.decode(response.body);
      return mapResponse;
    }catch(error){
      print(error);
    }
  }

  static methodPost({String link, String token, Map<String, dynamic> data}) async {
    final http.Response response = await http.post(link,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        body: json.encode(data),
    );
    final Map<String, dynamic> mapResponse = await json.decode(response.body);
    print(mapResponse);
    return mapResponse;
  }
}