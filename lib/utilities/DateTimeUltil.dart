import 'package:intl/intl.dart';

class DateTimeUtil{
  static formatDatetimeToString(String date){
      DateTime dateTime = DateTime.parse(date);
      final formatDate = new DateFormat('dd/MM/yyyy').format(dateTime);
      return formatDate;
  }
  static formatStringToDate(String date){
    try{
      DateTime dateTime = DateTime.parse("2018-11-03");
      final formatDate = new DateFormat("yyyy-MM-dd").format(dateTime);
      return formatDate;
    }catch(e) {
      print("GG: " + e.toString());
    }
  }
}