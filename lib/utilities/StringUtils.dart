import 'package:intl/intl.dart';
class StringUtils {

  static formatStringToDecemal(String number){
    final _formatter = new NumberFormat("#,###");
    String _numerFormated;
    try{
      if(number.isNotEmpty){
        _numerFormated = _formatter.format(int.parse(number));
      }
      return _numerFormated;
    }catch (error) {
      return "0bb";
    }
  }

  static convertStringToBool(String num){
    switch (int.parse(num)) {
      case 0:
        return false;
        break;
      case 1:
        return true;
        break;
      default:
        return false;
    }
  }

}